Handlebars.registerHelper("emptyString", function(str){
    return str != "";
});

Handlebars.registerHelper("readableDice", function(dice) {
    var str = "";
        
    if(valid(dice.no) && valid(dice.d))
        str += dice.no + "d" + dice.d;
    
    if(valid(dice.plus))
        str += "+" + dice.plus;
            
    function valid(x){
        return isNumber(x) && x > 0;
    }
    
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
        
    return str;
});