//Classes
function character(name, health)
{
    this.name = name;
    this.health = health;
    this.id = 0;
    this.statusEffects = [];
    this.inParty = false;
    this.priority = 0;
}

function statusEffect()
{
    this.name = "";
    this.dice = null;
    this.healing = false;
    this.duration = 0;
}

function dice()
{
    this.d = 0;
    this.no = 0;
    this.plus = 0;
}