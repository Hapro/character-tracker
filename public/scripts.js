var socket = io();
var sendCharacterCallback = function(){};
var id = 0;
var isOfficer = false; //ALways confirmed with server
var selectedCharacter = null;
var password = "";

$(document).ready(function() {
    hideOfficerInfo();
    registerSocketFunctions(socket);
    loadToolbarHTML();
});

function getJoinGameForm(){
    $("#initial-splash-form").fadeOut("slow", function(){
        $.get("joinGameForm.html", function(source) {
            $("#initial-splash-form").html(source);
            $("#initial-splash-form").fadeIn("slow");
        });
    });
}

function getNewGameForm(){
    $("#initial-splash-form").fadeOut("slow", function(){
        $.get("newGameForm.html", function(source) {
            $("#initial-splash-form").html(source);
            $("#initial-splash-form").fadeIn("slow");
        });
    });
}

function hideOfficerInfo(){
    $(".officer").fadeOut("slow");
}

function viewOfficerInfo(){
    $(".officer").fadeIn("slow");
    openCharacterManagementToolbar();
    tbNewCharacter();
}

function startNewGame(){
    password = $("#gameCreatePassword").val();
            
    //initial socket will override itself upon new game
    //or join game signal reception
    socket.on('gameID', function(newID){
        socket = io("/" + newID);
        $("#sub-title").html("Game ID: " + newID);
        $("#main-app").fadeIn("slow");
        $("#initial-splash").fadeOut("slow");
        $("#chatbox-row").fadeIn("slow");
        registerSocketFunctions(socket);
    });
    socket.emit("newGame", password);
}

function joinGame(){
    password = $("#gameJoinPassword").val();
    
    socket.on('gameID', function(newID){
        if(newID != -1){
            socket = io("/" + newID);
            $("#title").html("Game ID: " + newID);
            $("#main-app").fadeIn("slow");
            $("#initial-splash").fadeOut("slow");
            $("#chatbox-row").fadeIn("slow");
            registerSocketFunctions(socket);
        }
        else{
            $("#game-join-error").html("Error joining game!");
        }
    });
    
    socket.emit("joinGame", { id: $("#gameID").val(), password: password });
}


function registerSocketFunctions(socket){
    socket.on("setOfficer", function(officer){
        isOfficer = officer;
        if(officer)
            viewOfficerInfo();
        else
            hideOfficerInfo();
    });
    
    socket.on("setNameConfirm", function(){
        $("#chatbox-name-wrapper").fadeOut("slow");
    })
    
    socket.on('dc', function(cID){
        deleteCharacter(cID);
        socket.emit("updatePriorityList");
    });
    
    socket.on('nc', function(character){
        $.get("characterBox.html", function(source) {
            addCharacterBox(character, source);
        });
        socket.emit("updatePriorityList");
    });
    
    socket.on('uc', function(c){
        updateCharacter(c);
        socket.emit("updatePriorityList");
    });
    
    socket.on('updatePriorityList', function(pList){
        var html = "<h4>Priority <br>";
        for(var i = 0; i < pList.length; i++){
            if(pList[i].inParty){
                html += "<span class='label label-success inline-block'>" +
                    pList[i].name + " | " + pList[i].priority + "</span>";
            }
            else{
                html += "<span class='label label-danger inline-block'>" +
                    pList[i].name + " | " + pList[i].priority + "</span>";
            }
        }
        
        html += "</h4>"
        $("#priority-list").html(html);
    });
    
    //server sends a character
    socket.on('sc', function(character){
        sendCharacterCallback(character);
    });
    
    socket.on('sync', function(characters){
        console.log("sync");
        $("#characters").html("");
        $("#party-characters").html("");

        $.get("characterBox.html", function(source) {
            //create html for character
            for(var i = 0; i < characters.length; i++)
                addCharacterBox(characters[i], source);
        });
    });
    
    socket.on('chatmsg', function(msg){
        addTextMessageToTextBox(msg);
    });
}

function addCharacterBox(character, source){
    var template = Handlebars.compile(source);
    var context = 
        { 
            c: character,
            officer: isOfficer,
            all: true
        }
    var html = template(context);
    
    if(character.inParty){
        $("#party-characters").html($("#party-characters").html() + html);
    }
    else{
        $("#characters").html($("#characters").html() + html);
    }
}

function updateCharacter(c){
    $.get("characterBox.html", function(source) {
        var template = Handlebars.compile(source);
        var context = 
            { 
                c: c,
                officer: isOfficer,
                all: false
            }
        var html = template(context);
        $("#" + c.id + "_panel").html(html);
    });
}

function deleteCharacter(cID, callback){
    if(selectedCharacter == null || selectedCharacter.id == cID)
        selectedCharacter = null;
        
    $("#" + cID + "_panel").fadeOut("slow", callback);
}

function closeCharacterBox(el){
    socket.emit("dc", getCharacterIDfromElementID(el));
}

function select(el){
    var cID = el.id.substring(0, el.id.indexOf('_'));
    
    if(selectedCharacter == null || cID != selectedCharacter.id){
        getCharacterFromServer(cID,
            function(c){
                selectedCharacter = c; 
                refreshCharacterManagementToolbar();
            }
        );
    }
}

function requestSync(){ socket.emit("rsync"); }

function updateCharacterOnServer(character){
    socket.emit("uc", character);
}

function getCharacterFromServer(id, callback){
    sendCharacterCallback = callback;
    socket.emit('rc', id);
}

function addEffectButtonPressed(el){
    var characterID = getCharacterIDfromElementID(el);
    getCharacterFromServer(characterID, addEffectToCharacter);
}

function getCharacterElement(char, elName){ return $('#' + char.id + elName); }

function addEffectToCharacter(character){
    var effect = new statusEffect();
    effect.name = getCharacterElement(character, '_val_effect_name').val();
    effect.dpt = getCharacterElement(character, '_val_effect_dpt').val();
    effect.dice = new dice();
    effect.dice.d = getCharacterElement(character, '_val_dice_d').val();
    effect.dice.no = getCharacterElement(character, '_val_dice_no').val();
    effect.dice.plus = getCharacterElement(character, '_val_dice_plus').val();
    effect.healing = getCharacterElement(character, '_val_healing').is(':checked');
    effect.duration = getCharacterElement(character, '_val_duration').val();
    effect.color = getCharacterElement(character, '_effect-color-picker').spectrum("get").toHexString();
    effect.id = id++;
    effect.pid = character.id;
    character.statusEffects.push(effect);
    updateCharacterOnServer(character); 
}

//If element ID is in format {{c.id}}_x_x_... it can 
//get the front-trailing character id
function getCharacterIDfromElementID(el){ return el.id.substring(0, el.id.indexOf('_')); }

function removeEffectButtonPressed(el){
    var characterID = getCharacterIDfromElementID(el);
    var effectID = Number(el.id.substring(el.id.indexOf('_') + 1));
    
    getCharacterFromServer(characterID, function(c){
        var nEff = [];

        for(var i = 0; i < c.statusEffects.length; i++){
            if(c.statusEffects[i].id != effectID){
                nEff.push(c.statusEffects[i]);
            }
        }
       
       c.statusEffects = nEff;
       updateCharacterOnServer(c);
    });
}

function damButtonPressed(el){
    var characterID = getCharacterIDfromElementID(el);
    getCharacterFromServer(characterID, damageAndUpdateCharacter);
}

function damageAndUpdateCharacter(c){
    c.health -= $('#' + c.id + '_val_DAM').val();
    updateCharacterOnServer(c);
}

function updatePriorityButtonPressed(el){
    var characterID = getCharacterIDfromElementID(el);
    getCharacterFromServer(characterID, updateCharacterPriority);
}

function updateCharacterPriority(c){
    var newPriority = $("#" + c.id + "_val_priority").val();
    if (!isNaN(newPriority)){
        c.priority = newPriority;
        updateCharacterOnServer(c);
    }
}

function setOptionsBox(filename, character){
    $.get(filename, function(source) {
        var template = Handlebars.compile(source);
        var context = 
            { 
                c: character
            }
        var html = template(context);
        $("#options").html(html);
    });
}

function highlightPanel(el){
    //$(".highlight").removeClass("highlight");
    el.className += " highlight";
}

//Character management toolbar

var tbStatusEffectHTML;
var tbNewCharacterHTML;
var tbHTML;
var tbState = "";

function loadToolbarHTML(){
    $.get("characterEditorToolbar.html", function(source) {
        tbHTML = source;
    });
    $.get("addEffectForm.html", function(source) {
        tbStatusEffectHTML = source;
    });
    $.get("newCharacterForm.html", function(source) {
        tbNewCharacterHTML = source;
    });
}

function openCharacterManagementToolbar(){
    $("#sub-toolbar").html(tbHTML);
}

function clearCharacterManagementToolbarHTML(){
    $("#options-left").html("");
    $("#options-right").html("");
}

function tbStauts(){
    tbState = "tbStatus";
    clearCharacterManagementToolbarHTML();
    
    if(selectedCharacter != null){
        var template = Handlebars.compile(tbStatusEffectHTML);
        var context = 
            { 
                c: selectedCharacter
            }
        var html = template(context);
        $("#options-left").html(html);
        initializeColorPicker(selectedCharacter.id);
    }
}

function tbNewCharacter(){
    tbState = "tbNewCharacter";
    clearCharacterManagementToolbarHTML();
    $("#options-left").html(tbNewCharacterHTML);
}

function refreshCharacterManagementToolbar(){
    if (tbState == "tbStatus"){
        tbStauts();
    }
}

//New character 
function createNewCharacter(){
    var c = new character($('#name').val(), $('#hp').val());
    c.inParty = $('#chk_party').is(":checked");
    socket.emit('nc', c); 
}

//Messaging system
var col = true;

$("#chatbox-input").submit(function(event){
    event.preventDefault();
    sendTextMessage();
    return false;
})

function sendTextMessage(){
    var msg = 
    {
        text: $("#chatbox-input").val()
    };
    
    $("#chatbox-input").val("");
    socket.emit("chatmsg", msg);
    return false;
}

function addTextMessageToTextBox(msg){
    if(msg.admin){
        if(col)
            $("#chatbox").append("<div class='chatbox-text-stripe-1'><br><strong><span class='admin-chat-name'>" +
                msg.name + "</span>: </strong>" + msg.text + "</div>");
        else
            $("#chatbox").append("<div class='chatbox-text-stripe-2'><br><strong><span class='admin-chat-name'>" +
                msg.name + "</span>: </strong>" + msg.text + "</div>");
    }
    else{
        if(col)
            $("#chatbox").append("<div class='chatbox-text-stripe-1'><br><strong>" +
                msg.name + ": </strong>" + msg.text + "</div>");
        else
            $("#chatbox").append("<div class='chatbox-text-stripe-2'><br><strong>" + 
                msg.name + ": </strong>" + msg.text + "</div>");
    }
    
    //chat history of 100 messages
    if($("#chatbox").children().length > 100){
        $("#chatbox").children().first().remove();
    } 
    
    $('#chatbox').scrollTop($('#chatbox')[0].scrollHeight);
    
    col = !col;
}

function assignChatName(){
    socket.emit("setName",  $("#chatbox-name").val());
}


//Color picker code

function initializeColorPicker(id){
    $("#" + id + "_effect-color-picker").spectrum({
    });
}