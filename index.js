var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var express = require("express");
var IDGenerator = require('./IDGenerator.js');
var GameInstance = require('./gameInstance.js');

var gameInstances = [];
var dateTime = new Date();
var IDGen = new IDGenerator();

//for serving assets
app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(process.env.PORT, process.env.IP, function(){
  console.log('listening on *' + process.env.PORT);
  
  //delete inactive games every hour
  setInterval(function(){
      for(var i = 0; i < gameInstances.length; i++){
          if(gameInstances[i] && gameInstances[i].readyToDelete()){
              gameInstances[i] = null;
          }
      }
  }, 3600);
});

io.on('connection', function(socket){
    socket.on('newGame', function(password){
        var newID = IDGen.gen();
        var newIO = io.of('/' + newID);
        var gi = new GameInstance(newIO, newID);
        gi.setPassword(password);
        gameInstances.push(gi);
        socket.emit("gameID", newID);
    });
    
    socket.on('joinGame', function(joinData){
        var game = getGameByID(joinData.id);
        
        if(game != null && game.getPassword() == joinData.password)
            socket.emit("gameID", joinData.id);
        else
            socket.emit("gameID", -1);
    });
});

function getGameByID(id){
    for(var i = 0; i < gameInstances.length; i++){
        if(gameInstances[i] && gameInstances[i].id == id)
            return gameInstances[i];
    }
    
    return null;
}

function doesGameExist(games, id){
    for(var i = 0; i < games.length; i++){
        if(games[i] && games[i].id == id)
            return true;
    }
    
    return false;
}
