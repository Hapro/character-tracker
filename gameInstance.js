var IDGenerator = require('./IDGenerator.js');
var date = new Date();

function Player(id, socket, officer){
    this.id = id;
    this.socket = socket;
    this.officer = officer;
    this.name = "";
    this.lastMsg = 0;
    this.banned = false;
}

//class to handle individual instances of games
function GameInstance(uniqueSocket, id){
    var characters = [];
    var gIO = uniqueSocket;
    var IDGen = new IDGenerator();
    var players = [];
    var playersInGame = 0;
    var password = "";
    
    this.id = id;
    
    this.readyToDelete = function(){
        return playersInGame <= 0;
    }
    
    this.setPassword = function(pass){ password = pass; }
    
    this.getPassword = function(){ return password; }
    
    function addNormalSocketEvents(player){
        var socket = player.socket;
        
        socket.on("updatePriorityList", function(){
            var pl = [];

            for(var i = 0; i < characters.length; i++){
                pl.push(
                    {
                        name: characters[i].name,
                        priority: Number(characters[i].priority),
                        inParty: characters[i].inParty
                    }
                );
            }
            
            for(var i = 0; i < pl.length; i++){
                for(var x = 0; x < pl.length - 1; x++){
                    var temp;
                    if(pl[x].priority < pl[x+1].priority){
                        temp = pl[x+1];
                        pl[x+1] = pl[x];
                        pl[x] = temp;
                    }
                }
            }
                
           socket.emit("updatePriorityList", pl);
        });
        
        socket.on("newID", function(){
            socket.emit("newID", IDGen.gen());
        });
        
        //request character
        socket.on("rc", function(id){
            var c = getCharacterById(id);
            socket.emit('sc', c);
        });
        
        //request synchronisation to this client
        socket.on("rsync", function(){
            socket.emit('sync', characters);
        });
        
        //send chat message to players
        socket.on("chatmsg", function(msg){
            if(player.banned && !player.officer){
                msg.text = "You are banned!";
                msg.name = "Server";
                socket.emit("chatmsg", msg);
            }
            else if(player.name == ""){
                msg.text = "You must assign yourself a name first!";
                msg.name = "Server";
                socket.emit("chatmsg", msg);
            }
            else{
                var timeDiff = Date.now() - player.lastMsg;
                
                msg.admin = player.officer;
                
                if(player.officer && msg.text[0] == '/'){
                    processMacro(msg.text);
                }else{
                    //stop spam
                    if(timeDiff > 700){
                        msg.name = player.name;
                        player.lastMsg = Date.now();
                        
                        //strip html
                        msg.text = msg.text.replace(/<(?:.|\n)*?>/gm, '');
                        
                        gIO.emit('chatmsg', msg);
                    }
                    else{
                        msg.name = "Server";
                        msg.text = "<span class='warn'>You must wait at least 700ms before doing that!</span>";
                        socket.emit("chatmsg", msg);
                    }
                }
            }
        });
        
        socket.on("setName", function(name){
            if(player.name == ""){ //only assign name if unset
                if(uniqueName(name)){
                    player.name = name;
                    var msg = { name: "Server", text: "User " + name + " joined chat!" };
                    gIO.emit("chatmsg", msg);
                    socket.emit("setNameConfirm");
                }
                else{
                    var msg = { name: "Server", text: "Name already in use!" };
                    socket.emit("chatmsg", msg);
                }
            }
        });
        
        socket.on("disconnect", function(){
           playersInGame--;
           players[player.id] = null;
        });
    }
    
    function addAdminSocketEvents(player){
        var socket = player.socket;
        
        //new characater
        socket.on("nc", function(nc){
            nc.id = IDGen.gen(); //assign ID to character
            characters.push(nc);
            gIO.emit('nc', nc);
        });
        
        //delete characater
        socket.on("dc", function(id){
            var newArr = [];
            
            for(var i = 0; i < characters.length; i++)
            {
                if(characters[i].id != id)
                    newArr.push(characters[i]);
            }
            
            characters = newArr;
            gIO.emit('dc', id);
        });
        
        //update characater
        socket.on("uc", function(nc){
            for(var i = 0; i < characters.length; i++){
                if(characters[i].id == nc.id)
                    characters[i] = nc;
            }
            gIO.emit('uc', nc);
        });
    }
    
    gIO.on("connection", function(socket){
        var isOfficer = players.length == 0;
        var player = new Player(IDGen.gen(), socket, isOfficer);
        addNormalSocketEvents(player);
        
        if(isOfficer == true){
            addAdminSocketEvents(player);
            socket.emit("chatmsg", 
                {
                    text: "Welcome to the character tracker beta. Use /ban [user] to ban rude players. <strong style='color: red'>This game's" +
                    " ID is " + id + ".</strong>",
                    name: "Sever"
                }
            );
        }
        
        socket.emit("setOfficer", player.officer);
        socket.emit("sync", characters);
        //use ID to hashmap players
        players[player.id] = player;
        
        playersInGame++;
    });
    
    //search for a character with given ID
    function getCharacterById(id){
        for(var i = 0; i < characters.length; i++){
            if(characters[i].id == id)
                return characters[i];
        }
        
        return null;
    }
    
    function toggleBan(playerName){
        for(var i = 0; i < players.length; i++){
            if(players[i] && players[i].name == playerName)
                players[i].banned = !players[i].banned;
        }
    }
    
    function uniqueName(playerName){
        for(var i = 0; i < players.length; i++){
            if(players[i] && players[i].name == playerName)
                return false;
        }
        
        return true;
    }
    
    function processMacro(text){
        console.log(firstWord(text) + " " + secondWord(text));
        if(firstWord(text) == "/ban"){
            toggleBan(secondWord(text));
        }
    }
}

module.exports = GameInstance;

function firstWord(text){
    return text.substring(0, text.indexOf(' '));
}

function secondWord(text){
    return text.substring(text.indexOf(' ') + 1);
}