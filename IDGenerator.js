function IDGenerator(){
    this.value = 0;

    this.gen=function(){
        var returnValue = this.value;
        this.value++;
        return returnValue;
    };
}

module.exports = IDGenerator;
